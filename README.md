# Node.js Developers Challenge
Microservicio desarrollado en Node.js v16 usando su propio servidor HTTP.

Para una Progressive Web Application en Vue.js: [https://gitlab.com/evicentemedina/vue-developers-challenge](https://gitlab.com/evicentemedina/vue-developers-challenge)

PDF con los requisitos: [http://vuedevc.evime.es/DevelopersChallengeFront.pdf](http://vuedevc.evime.es/DevelopersChallengeFront.pdf)

## Disponible en
[http://vuedevc.evime.es/](http://vuedevc.evime.es/)

## Instrucciones de despliegue
Son necesarios [Node.js](https://nodejs.org/) v16 o superior y [NPM](https://www.npmjs.com/) v7 o superior.

### En local para desarrollo
```
npm start
```

### En producción
```
npm run prod
```

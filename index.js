import { networkInterfaces } from 'os'
import { createServer } from 'http'

const prod = process.argv.slice(2)[0] === 'prod'
console.log('Production', prod)

function getInterfaceAddress() {
  const nets = networkInterfaces();
  for (const name of Object.keys(nets)) {
    for (const net of nets[name]) {
      if (net.family === 'IPv4' && !net.internal) {
        return net.address;
      }
    }
  }
  return 'localhost'
}

const host = prod ? getInterfaceAddress() : 'localhost'
const port = 8081
const methods = 'OPTIONS, GET, POST'
const headers = [
  ['Access-Control-Allow-Origin', prod ? 'http://vuedevc.evime.es' : '*'],
  ['Content-Type', 'application/json; charset=utf-8'],
  ['Allow', methods],
  ['Access-Control-Allow-Methods', methods],
  ['Access-Control-Allow-Headers', 'X-PINGOTHER, Content-Type'],
  ['Access-Control-Max-Age', '86400'],
]

const userLists = []

function setHeaders(res) {
  for (const header of headers) {
    res.setHeader(...header)
  }
}

createServer((req, res) => {
  console.log(req.method, 'request')
  switch (req.method) {
    case 'OPTIONS':
      res.statusCode = 200
      setHeaders(res)
      res.end()
      return
    case 'GET':
      res.statusCode = 200
      setHeaders(res)
      res.write(JSON.stringify(userLists))
      res.end()
      return
    case 'POST':
      process.stdout.write('Recieving POST payload')
      let payload = ''
      req.on('data', (chunk) => {
        process.stdout.write('.')
        payload += chunk
      })
      req.on('end', () => {
        let userList = JSON.parse(payload)
        let valid = Boolean(userList && userList.name && userList.name.length > 0 && userList.name.length < 10
            && userList.users && userList.users.length)
        let nameExists = userLists.find(el => el.name === userList.name)
        console.log('\nPOST payload recieved. Valid:', valid)
        if (valid && !nameExists) {
          userLists.push({
            name: userList.name,
            users: userList.users,
          })
          res.statusCode = 201
          setHeaders(res)
          res.end()
        } else
        if (nameExists) {
          const msg = 'Nombre de listado ya existente'
          console.warn(msg)
          res.statusCode = 400
          setHeaders(res)
          res.write(JSON.stringify(msg))
          res.end()
        } else {
          const msg = 'Listado no válido'
          console.warn(msg)
          res.statusCode = 400
          setHeaders(res)
          res.write(JSON.stringify(msg))
          res.end()
        }
      })
      req.on('error', error => {
        console.error(error)
        res.statusCode = 500
        setHeaders(res)
        res.write(JSON.stringify('Error del servidor'))
        res.end()
      })
      return
    default:
      res.statusCode = 200
      setHeaders(res)
      res.end()
      return
  }
}).listen(port, host, () => {
  console.log(`User lists HTTP microservice running at http://${host}:${port}/`)
})
